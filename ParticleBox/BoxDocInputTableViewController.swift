//
//  BoxDocInputTableViewController.swift
//  ParticleBox
//
//  Created by Stuart Levine on 4/15/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SLParticleBoxApi

enum CellInputType {
    case text
    case integer
    case segmented
}

class BoxDocCellViewModel {
    var inputType: CellInputType
    var ident: String
    var displayName: String
    var text = BehaviorRelay<String>(value: "")
    var selectedSegmentIndex = BehaviorRelay<Int>(value: 0)

    init(inputType: CellInputType, inputIdent: String, displayName: String) {
        self.inputType = inputType
        self.ident = inputIdent
        self.displayName = displayName
    }
}

class BoxDocTableViewCell: UITableViewCell {
    let bag = DisposeBag()
    var viewModel: BoxDocCellViewModel?
    let inputLabel = UILabel(font: UIFont.systemFont(ofSize: 12), color: .lightGray)

    lazy var textField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 18)
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textField.isHidden = true
        textField.backgroundColor = UIColor.black.withAlphaComponent(0.05)

        return textField
    }()

    lazy var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: DetailViewController.displayScopes)
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0
        control.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        control.isHidden = true

        return control
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        contentView.addSubview(inputLabel)
        contentView.addSubview(textField)
        contentView.addSubview(segmentedControl)

        inputLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        inputLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -50).isActive = true
        inputLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true

        textField.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        textField.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 6).isActive = true
        segmentedControl.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        segmentedControl.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        segmentedControl.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 6).isActive = true
    }

    func bind(to viewModel: BoxDocCellViewModel) {
        self.viewModel = viewModel
        inputLabel.text = viewModel.displayName

        switch viewModel.inputType {
        case .text, .integer:
            textField.isHidden = false
            textField.rx.text.orEmpty.bind(to: viewModel.text).disposed(by: bag)
        case .segmented:
            segmentedControl.isHidden = false
            segmentedControl.rx.selectedSegmentIndex.bind(to: viewModel.selectedSegmentIndex).disposed(by: bag)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        textField.text = ""
        textField.isHidden = true
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.isHidden = true
    }
}

protocol BoxDocInputTableViewControllerDelegate : class {
    func didAddNewBoxDocument(_ viewController: BoxDocInputTableViewController, document: BoxDocument)
}

class BoxDocInputTableViewController: UITableViewController {
    weak var delegate: BoxDocInputTableViewControllerDelegate?

    let boxDocCellIdent = "boxDocCellIdent"
    var boxDocument = BoxDocument()

    let names = ["Key", "Value", "Scope", "Device Id", "Product Id"]
    let idents = ["key", "value", "scope", "device_id", "product_id"]
    let scopes = DetailViewController.displayScopes.map { $0.lowercased() }

    lazy var dataSource: [BoxDocCellViewModel] = {
        var dataSource = [BoxDocCellViewModel]()
        for (index, value) in idents.enumerated() {
            var inputType: CellInputType = .text
            if value == "scope" {
                inputType = .segmented
            }
            else if value == "product_id" {
                inputType = .integer
            }
            let viewModel = BoxDocCellViewModel(inputType: inputType, inputIdent: value, displayName: names[index])
            dataSource.append(viewModel)
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(BoxDocTableViewCell.self, forCellReuseIdentifier: boxDocCellIdent)
        tableView.allowsSelection = false

        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveDocument(_:)))
        navigationItem.rightBarButtonItem = saveButton
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel(_:)))
        navigationItem.leftBarButtonItem = cancelButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc
    func saveDocument(_ sender: UIBarButtonItem) {
        var dict = [String: Any]()

        for (index, value) in idents.enumerated() {
            let viewModel = dataSource[index]
            switch viewModel.inputType {
            case .text:
                dict[value] = viewModel.text.value
            case .integer:
                dict[value] = Int(viewModel.text.value)
            case .segmented:
                dict[value] = scopes[viewModel.selectedSegmentIndex.value]
            }
        }
        if let data = try? JSONSerialization.data(withJSONObject: dict, options: []), let boxDocument = try? SLParticleBoxSDK.jsonDecoder.decode(BoxDocument.self, from: data) {
            boxDocument.updatedAtString = boxDocument.dateFormatter.string(from: Date())
            delegate?.didAddNewBoxDocument(self, document: boxDocument)
            dismiss(animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController.createWith(title: "Error", message: "Could not save document.  Please check your input and try again", handler: nil)
            present(alertController, animated: true, completion: nil)
        }
    }

    @objc
    func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: boxDocCellIdent, for: indexPath)
        let inputCell = cell as? BoxDocTableViewCell
        let viewModel = dataSource[indexPath.row]
        inputCell?.bind(to: viewModel)
        return cell
    }
}

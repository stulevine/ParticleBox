//
//  MasterTableViewCell.swift
//  ParticleBox
//
//  Created by Stuart Levine on 4/15/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import UIKit

let kMasterTableViewCellIdent = "kMasterTableViewCellIdent"

class MasterTableViewCell: UITableViewCell {
    let titleLabel = UILabel(font: UIFont.systemFont(ofSize: 16), color: .darkGray)
    let subtitleLabel = UILabel(font: UIFont.systemFont(ofSize: 14), color: .lightGray)
    let scopeLabel = UILabel(font: UIFont.systemFont(ofSize: 14), color: .lightGray, textAlignment: .right)

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: kMasterTableViewCellIdent)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(scopeLabel)

        self.accessoryType = .disclosureIndicator

        scopeLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        subtitleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        subtitleLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: scopeLabel.leftAnchor, constant: -10).isActive = true
        scopeLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -5).isActive = true
        scopeLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -10).isActive = true
        subtitleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 10).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
        subtitleLabel.text = nil
        scopeLabel.text = nil
    }

}

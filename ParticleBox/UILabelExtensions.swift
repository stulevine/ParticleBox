//
//  UILabelExtensions.swift
//  ParticleBox
//
//  Created by Stuart Levine on 4/15/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel {
    convenience init(font: UIFont, color: UIColor = UIColor.darkGray, translatesAutoresizingMaskIntoConstraints: Bool = false, text: String? = nil, textAlignment: NSTextAlignment = .left) {
        self.init(frame: .zero)
        self.font = font
        self.textColor = color
        self.text = text
        self.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints
        self.heightAnchor.constraint(equalToConstant: font.pointSize)
        self.lineBreakMode = .byTruncatingTail
        self.textAlignment = textAlignment
    }
}


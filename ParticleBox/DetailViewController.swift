//
//  DetailViewController.swift
//  ParticleBox
//
//  Created by Stuart Levine on 4/14/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import UIKit
import SLParticleBoxApi

class DetailViewController: UIViewController {

    static let lablelFontSize: CGFloat = 12
    static let dataFontSize: CGFloat = 20
    let verticalSpacing: CGFloat = 25
    let leftRightPadding: CGFloat = 20
    let interimSpacing: CGFloat = 5

    static let displayScopes = ["Device", "User", "Product"]
    let scopes = DetailViewController.displayScopes.map { $0.lowercased() }
    // labels
    let valueTitleLabel = UILabel(font: .systemFont(ofSize: lablelFontSize), color: UIColor.black.withAlphaComponent(0.35), text: "Value:")
    let scopeTitleLabel = UILabel(font: .systemFont(ofSize: lablelFontSize), color: UIColor.black.withAlphaComponent(0.35), text: "Scope:")
    let deviceIdTitleLabel = UILabel(font: .systemFont(ofSize: lablelFontSize), color: UIColor.black.withAlphaComponent(0.35), text: "Device Id:")
    let productIdTitleLabel = UILabel(font: .systemFont(ofSize: lablelFontSize), color: UIColor.black.withAlphaComponent(0.35), text: "Product Id:")
    let updatedAtTitleLabel = UILabel(font: .systemFont(ofSize: lablelFontSize), color: UIColor.black.withAlphaComponent(0.35), text: "Last Updated:")

    // data
    let valueLabel = UILabel(font: UIFont.systemFont(ofSize: dataFontSize))
    let scopeSegment: UISegmentedControl = {
        let control = UISegmentedControl(items: displayScopes)
        control.translatesAutoresizingMaskIntoConstraints = false
        control.isUserInteractionEnabled = false
        control.tintColor = .darkGray
        control.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return control
    }()
    let deviceIdLabel = UILabel(font: UIFont.systemFont(ofSize: dataFontSize))
    let productIdLabel = UILabel(font: UIFont.systemFont(ofSize: dataFontSize))
    let updatedAtLabel = UILabel(font: UIFont.systemFont(ofSize: dataFontSize))

    var document: BoxDocument?

    override func awakeFromNib() {
        super.awakeFromNib()

        setupViews()
    }

    func setupViews() {
        view.addSubview(valueTitleLabel)
        view.addSubview(valueLabel)
        view.addSubview(scopeTitleLabel)
        view.addSubview(scopeSegment)
        view.addSubview(deviceIdTitleLabel)
        view.addSubview(deviceIdLabel)
        view.addSubview(productIdTitleLabel)
        view.addSubview(productIdLabel)
        view.addSubview(updatedAtTitleLabel)
        view.addSubview(updatedAtLabel)

        valueTitleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: verticalSpacing).isActive = true
        valueTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        valueTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
        valueLabel.topAnchor.constraint(equalTo: valueTitleLabel.bottomAnchor, constant: interimSpacing).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        valueLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true

        scopeTitleLabel.topAnchor.constraint(equalTo: valueLabel.bottomAnchor, constant: verticalSpacing).isActive = true
        scopeTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        scopeTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
        scopeSegment.topAnchor.constraint(equalTo: scopeTitleLabel.bottomAnchor, constant: interimSpacing).isActive = true
        scopeSegment.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        scopeSegment.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true

        deviceIdTitleLabel.topAnchor.constraint(equalTo: scopeSegment.bottomAnchor, constant: verticalSpacing).isActive = true
        deviceIdTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        deviceIdTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
        deviceIdLabel.topAnchor.constraint(equalTo: deviceIdTitleLabel.bottomAnchor, constant: interimSpacing).isActive = true
        deviceIdLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        deviceIdLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true

        productIdTitleLabel.topAnchor.constraint(equalTo: deviceIdLabel.bottomAnchor, constant: verticalSpacing).isActive = true
        productIdTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        productIdTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
        productIdLabel.topAnchor.constraint(equalTo: productIdTitleLabel.bottomAnchor, constant: interimSpacing).isActive = true
        productIdLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        productIdLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true

        updatedAtTitleLabel.topAnchor.constraint(equalTo: productIdLabel.bottomAnchor, constant: verticalSpacing).isActive = true
        updatedAtTitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        updatedAtTitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
        updatedAtLabel.topAnchor.constraint(equalTo: updatedAtTitleLabel.bottomAnchor, constant: interimSpacing).isActive = true
        updatedAtLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftRightPadding).isActive = true
        updatedAtLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -leftRightPadding).isActive = true
    }

    func bind(to document: BoxDocument?) {
        self.document = document

        if let document = document {
            print(document)
            self.title = document.key ?? "Box Document"

            valueLabel.text = document.value
            if let scope = document.scope, let index = scopes.index(of: scope) {
                scopeSegment.selectedSegmentIndex = index
            }
            deviceIdLabel.text = document.deviceId
            if let productId = document.productId {
                productIdLabel.text = "\(productId)"
            }
            updatedAtLabel.text = document.longDateString
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

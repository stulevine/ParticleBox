//
//  MasterViewController.swift
//  ParticleBox
//
//  Created by Stuart Levine on 4/14/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import UIKit
import SLParticleBoxApi

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var dataSource = [BoxDocument]()
    var isLoadingData = false
    lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.delegate = self
        controller.searchResultsUpdater = self
        controller.hidesNavigationBarDuringPresentation = false
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.barStyle = .default
        controller.searchBar.sizeToFit()

        return controller
    }()
    var isSearching: Bool {
        return searchController.isActive && (searchController.searchBar.text?.count ?? 0) > 0
    }
    var searchResults = [BoxDocument]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Box Documents"

        SLParticleBoxSDK.configure(withApiKey: "XXXXXXXXXXXXX") // configure the API faux Key

        loadData()

        refreshControl = UIRefreshControl(frame: .zero)
        refreshControl?.addTarget(self, action: #selector(reloadData(_:)), for: .valueChanged)

        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewDocument(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    func loadData() {
        isLoadingData = true
        let filter = SLParticleBoxSDK.PBFilter(perPage: 10)
        SLParticleBoxSDK.apiCall(with: .list(filter)) { [weak self] (response) in
            guard let `self` = self else { return }
            self.isLoadingData = false

            switch response {
            case .success(let response):
                if let response = response {
                    self.dataSource = response
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.refreshControl?.endRefreshing()
                    }
                }
            case .error(let error):
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                }
                let message = error?.localizedDescription ?? "Unknown error."
                let alert = UIAlertController.createWith(title: "Error", message: message, handler: nil)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @objc
    func reloadData(_ sender: UIRefreshControl) {
        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc
    func addNewDocument(_ sender: Any) {
        let vc = BoxDocInputTableViewController(style: .plain)
        vc.delegate = self
        let nc = UINavigationController(rootViewController: vc)
        present(nc, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let document = dataSource[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.bind(to: document)
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return !isSearching ? dataSource.count : searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let document = !isSearching ? dataSource[indexPath.row] : searchResults[indexPath.row]

        let masterCell = cell as? MasterTableViewCell
        masterCell?.titleLabel.text = document.key
        masterCell?.subtitleLabel.text = "Device Id: \(document.deviceId ?? "n/a")"

        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return !isSearching
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let confirmDeleteAlert = UIAlertController(title: "Confirmation",
                                                       message: "You are about to delete a document.  This action can't be undone. Are you sure?",
                                                       preferredStyle: .alert)
            
            let yesButton = UIAlertAction(title: "Yes", style: .destructive) { [weak self] (_)in
                guard let `self` = self else { return }
                let document = self.dataSource[indexPath.row]
                if let key = document.key {
                    SLParticleBoxSDK.apiCall(with: .delete(key: key), onCompletion: { (response) in
                        switch response {
                        case .success(_):
                            self.dataSource.remove(at: indexPath.row)
                            DispatchQueue.main.async {
                                tableView.deleteRows(at: [indexPath], with: .fade)
                            }
                        case .error(let error):
                            let errorAlert = UIAlertController.createWith(title: "Error",
                                                                          message: error?.localizedDescription ?? "An unexpected error occurred.", handler: { (_) in
                                                                            // just leave things as is
                            })
                            DispatchQueue.main.async {
                                self.present(errorAlert, animated: true, completion: nil)
                            }
                        }
                    })
                }
            }

            let noButton = UIAlertAction(title: "No", style: .default) { (_) in
                // do nothing
            }
            confirmDeleteAlert.addAction(yesButton)
            confirmDeleteAlert.addAction(noButton)
            present(confirmDeleteAlert, animated: true, completion: nil)
        }
    }
}

extension MasterViewController: BoxDocInputTableViewControllerDelegate {
    func didAddNewBoxDocument(_ viewController: BoxDocInputTableViewController, document: BoxDocument) {
        SLParticleBoxSDK.apiCall(with: .create(document)) { [weak self] (response) in
            guard let `self` = self else {
                return
            }
            switch response {
            case .success(_):
                self.dataSource.append(document)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .error(let error):
                let message = error?.localizedDescription ?? "Unknown error."
                let alert = UIAlertController.createWith(title: "Error", message: message, handler: nil)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension MasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        searchResults = dataSource.filter({ (doc) -> Bool in
            guard let searchText = searchController.searchBar.text?.lowercased(), let key = doc.key?.lowercased() else { return false }
            return key.contains(searchText)
        })
        tableView.reloadData()
    }
}

extension MasterViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        navigationItem.leftBarButtonItem?.isEnabled = true
        navigationItem.rightBarButtonItem?.isEnabled = true
    }

    func didPresentSearchController(_ searchController: UISearchController) {
        navigationItem.leftBarButtonItem?.isEnabled = false
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
}

typealias AlertActionHandler = (UIAlertAction)->()

extension UIAlertController {
    static func createWith(title: String, message: String, handler: AlertActionHandler?) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(okAction)

        return alertController
    }
}
